﻿# RESUMO DISCIPLINA - PROGRAMAÇÃO PARA INTERNET
### PROJETO DE DESENVOLVIMENTO - PROTOTIPAÇÃO 

[![N|Solid](https://pbs.twimg.com/profile_images/815895585417023488/_zfDlgKc.jpg)](http://www.fgf.edu.br/)

## X FEIRA TECNOLÓGICA DA FGF
### II MOSTRA CIENTIFICA FACULDADE INTEGRADA DA GRANDE FORTALEZA 2017 
#### TEMA: "Cidades Inteligentes"

Realizado criação de resumo na plataforma Sharelatex mediante pesquisa do Tema proposto.
- Resumo em Tex 
- Prototipo em HTML e CSS
- Prototipo do Layout Conceitual
- Referências de pesquisa 


## Equipe 

| NOME | EMAIL |
|-----| ------|
| Cleilson | cleilsonpereira@aluno.fgf.edu.br|
| Adrielle | adriellediogo@aluno.fgf.edu.br |
| Eder Sousa | ederss@aluno.fgf.edu.br |
| Jefferson | jefferson@aluno.fgf.edu.br |

